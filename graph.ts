import * as fs from 'fs';
import { xml2js } from 'xml-js';
import { get as getByKeyPath} from 'lodash';
// const getByKeyPath = require('lodash.get');     // Lodash is being a peach.. 

const exists = (obj: any) => !(obj === undefined || obj === null);

const deepExists = (obj: any, ...keys: string[]) => exists(getByKeyPath(obj, keys))

enum EdgeLabel {
    PRODUCES = 'Produces',
    REQUIRES = 'Requires' 
}

interface IEtcKey {
    [key:string]: INode;
    Activity: INode;
    Category: INode;
    Object: INode;
    Protocol: INode;
    Dataset: INode;
}

interface IEdge {
    [key: string]: INode | string;
    fromRef: string;
    toRef: string;
    type: string;
}

// The mxCell in the drawIO diagram
interface ICell {
    [key:string]: string;
}

interface ITypeValue {
    type: string | undefined;
    value: string | undefined;
}

interface INode {
    [key: string]: string | undefined;
    id: string,
    parent: string,
    source: string,
    target: string,
    value: string,
    color?: string,
    type?: string,
}

class Graph {
    public readonly nodes: INode[];
    public readonly edges: IEdge[];
    public readonly etcKey: IEtcKey;

    constructor(path: string, relationTypes?: string[], entKey?: string[]) {
        const entityKey = entKey || ['Activity', 'Category', 'Object', 'Protocol', 'Dataset'];
        const relTypes = relationTypes || ['Produces', 'Requires'];

        // Graph vertecies
        this.nodes = this.buildINodes(path);                 
        // Graph edges
        this.edges = this.buildEdges(relTypes, this.nodes);
        // Entity Type Key from graph
        this.etcKey = this.buildEtcKey(entityKey, this.nodes);
    }

    public byId = (nodeId: string) => (nodeId ? this.nodes.find(node => node.id === nodeId) : undefined)

    public getEdgeLines = (): INode[] => this.nodes.filter(n => exists(n.source) && exists(n.target));

    public getEdgeLabels = (): INode[] => this.nodes.filter(node => ['Produces', 'Requires'].indexOf(node.value) >= 0);

    public filterEntities = (): INode[] => this.nodes.filter(n => exists(n.color));

    public getEdgesByRelType = (relType: string) => this.edges.filter(edge => edge.type === relType);

    private buildINodes = (path: string): INode[] => {
        const root = this.readDrawIo(path);
        return this.normalizeCells(root);
    }

    // Reads from draw io diagram xml
    private readDrawIo = (path: string) => { 
        const xml = fs.readFileSync(path).toString();
        const diagram = xml2js(xml, { compact: true, ignoreDeclaration: true }); 

        if(!deepExists(diagram, 'mxGraphModel', 'root')) {
            throw Error('Need to have a graph root to continue parsing');
        }
        return diagram.mxGraphModel.root;
    }

    // In case one of the cells gets stuck in an object wrapper. Flatten the object into mxCell and carry the ID.
    private normalizeCells = (root: any): INode[] => {
        const getColor = (cell: ICell) => {
            if (!deepExists(cell, 'style') || !cell.style.includes('fillColor')) { 
                return undefined;
            }
            const fillColor: string = cell.style.split(';').find(s => s.startsWith("fillColor"))!;
            const [,color] = fillColor.split('=');
            return color;
        }

        return this.mergeObjectAndMxCell(root)
                    .map((cell: any) => ({...cell._attributes, color: getColor(cell._attributes)}));
    }

    private mergeObjectAndMxCell = (root: any) => {
        const buildObjectCell = (obj: any) => ({ _attributes: { ...obj.mxCell._attributes, id: obj._attributes.id } });
        if (exists(root.object) && exists(root.mxCell)) {
            if (Array.isArray(root.object)) {
                return root.mxCell.concat(root.object.map(buildObjectCell));
            }
            else {
                return root.mxCell.concat(buildObjectCell(root.object));
            }
        }
        return root.mxCell; 
    }

    private buildEdges(labelNames: string[], nodes: INode[]) {
        // Builds an edge from label(aka relation type) and relation
        const buildEdge = (label: string, rel: INode) => {
            const parent = this.byId(rel.parent);
            if(!exists(parent)) { 
                return undefined; 
            };
            return {
                type: label,
                fromRef: parent!.source,
                toRef: parent!.target
            };
        };

        return labelNames.reduce((memo, label) => {
            const relLabelINodes: INode[] = nodes.filter(node => node.value === label);
            return memo.concat(relLabelINodes.map(buildEdge.bind(this, label)));
        }, []);
    }

    // Parses the color scheme for ETC key to used in identifying entities
    private buildEtcKey(targets: string[], nodes: INode[]) { 
        const etcKey = this.nodes.reduce((memo, node) => {
            const candidate:string = node.value;

            if (targets.indexOf(candidate) >= 0 && !exists(memo[candidate])) {
                memo[candidate] = node;
            }

            return memo;
        }, {} as IEtcKey);

        // Need a parsed etc key to update types
        this.updateINodeTypes(etcKey); 

        return etcKey;
    }

    private updateINodeTypes = (etcKey: IEtcKey) => this.nodes.forEach(
        (node: INode) => node.type = this.findEntityByColor(node.color!, etcKey)
    );

    private findEntityByColor(color: string | undefined, etcKey: IEtcKey): string | undefined {
        if (!exists(color) || !exists(etcKey)) {
            return undefined;
        }

        return Object.keys(etcKey).find((key: string) => {
            return etcKey[key] && etcKey[key].color == color;
        });
    }
}

const graph= new Graph('samples/mid_level_example.xml');
console.log(graph);

// Go by  S = requires => Act => produces Data 
// 
const gwt = (graph: Graph): void => {
    const isActivity = (node: INode): boolean => node.type === "Activity";
    const isInput = (node: INode): boolean => node.type === "Object" || node.type === "Dataset";

    const functify = (fName: string, val: string, cbText: string) => `${fName}('${val}, ${cbText});'`;

    // const requiredBy = (rel: IEdge): {given: ITypeValue, when:ITypeValue } | undefined =>  {
    //     if(!isInput(rel.fromRef)) {
    //         return;
    //     } 
    //     if(!isActivity(rel.toRef)){
    //         return;
    //     }

    //     return {
    //         given: {
    //             type: rel.fromRef.type,
    //             value: rel.fromRef.value
    //         },
    //         when: {
    //             type: rel.toRef.type,
    //             value: rel.toRef.value
    //         }
    //     };
    // }

    function produces(rel: IEdge) {
        // gather by fromRef
    }

    const requires = (edges: IEdge[]) => {
        const grouped = edges.reduce((memo, edge) => { 
            if(!exists(memo[edge.toRef])) {
                memo[edge.toRef] = Array.of(edge.fromRef);
            } else {
                memo[edge.toRef].push(edge.fromRef);
            }

            return memo;
        }, {} as {[key:string]: any});
        const setInput = '(doc) => { this.input = JSON.parse(doc); }';

        console.log(grouped);

        const target = Object.keys(grouped)[0];

        const given = Object.keys(grouped).map((toRef: string) => {
            const to = graph.byId(toRef);
            // return functify('Given', from!.value , setInput);
        }); 

        console.log(given);

        // Gather by toRef 
        // act1 requires: s1 s2 s3
        // act2 requires: s3 s2 s1
    }


    function givenWhenThen(graph: Graph) {
        // const relations = _.zip(graph.['Required By'], Produces.Produces); //filter(val > val;
        const requiredBy = graph.edges.filter(e => e.type === EdgeLabel.REQUIRES);

        requires(requiredBy);

        const produces = graph.edges.filter(e => e.type === EdgeLabel.PRODUCES);
       
        let steps =  "import { Given, Then } from 'cucumber';\n\n";

        // produces = then
        console.log(steps);
    }
    givenWhenThen(graph);
}
gwt(graph);

//
// relation.activity-sample
// produces
// requires
// treats

// relation.activity-result
// produces
// requires

// relation.activity-activity
// includes
// describes